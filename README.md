# nfs-dirmaker

Creates a directory into a NFS server exported file system.
We use it to create the users' home directory before trying the mount via autofs.

## Requirements

* Python 3.8+
* Flask 2.1
* gunicorn

### Build the environment

``` shell
python3 -m venv venv
. venv/bin/activate
pip install Flask
```

### Test the service

``` shell
source ./venv/bin/activate
export FLASK_APP=dircreate
export FLASK_ENV=development
flask run
```

Use gunincorn when in production
