#!/usr/bin/env python3

"""This code creates a directory named after the user.

When a user starts using one of the shinyproxy services, a home directory will
be created in the NFS server if it does not exist yet.
The home directory will be then mounted by the shinyproxy service inside the
application container.
"""

import configparser
import os
from os.path import exists
from flask import Flask
from flask import request

debug = False

if debug:
  config_path_dir = '/tmp'
else:
  config_path_dir = './instance'
  config_filename = f'{config_path_dir}/config.ini'

if exists(config_filename):
  config = configparser.ConfigParser()
  config.read(config_filename)
  if debug:
    print('Sections found in the config file: ', config.sections())
  if 'create-homedir' in config.sections():
    for key in config['create-homedir']:
      if debug:
        print('Key: ', key)
      if config['create-homedir']['SECRET_KEY']:
        SECRET_KEY = config['create-homedir']['SECRET_KEY']
      else:
        raise SystemExit('Error: config file, no SECRET_KEY found.')
      if config['create-homedir']['DIR_PREFIX']:
        DIR_PREFIX = config['create-homedir']['DIR_PREFIX']
      else:
        raise SystemExit('Error: config file, no DIR_PREFIX found.')
  else:
    raise SystemExit('Error: no create-homedir section found.')
else:
  raise SystemExit('Error: no config file found.')


def make_homedir(dir_path):
  """Create the directory if it does not exist.
  """
  if not os.path.isdir(dir_path):
    permissions = 0o700
    try:
      os.mkdir(dir_path, permissions)
    except OSError:
      return(f'Creation of the directory {dir_path} failed'), 500
    else:
      return(f'Directory {dir_path} created successfully'), 200
  return(f'Directory {dir_path} already exists'), 200


def create_app(test_config=None):
  """Initialize the service and start it.
  """
  # create and configure the app
  create_home = Flask(__name__, instance_relative_config=True)
  # ensure the instance folder exists
  try:
    os.path.isdir(DIR_PREFIX)
  except OSError:
    pass

  @create_home.route('/', methods=['GET', 'POST'])
  def makedir():
    if request.method == 'POST':
      username = request.form['user']
      return make_homedir(f'{DIR_PREFIX}/{username}')
    else:
      return "Nosey, aren't you?"

  return create_home
